<style>
    .content {
        min-height: calc(100vh - 120px);
    }

    .footer {
        display: flex;
        flex-direction: column;
        align-items: center;
        font-weight: bold;
        font-size: 14px;
        background-color: #33283c26;
        color: #3d34c3a3;
        padding: 18px 24px;
        text-align: center;
        margin-bottom: 0px;
    }

    .footer a {
        text-decoration: none;
        color: #3d34c3a3
    }
    .footer-list{
        margin-bottom: 10px;
        color: #3d34c3a3;
    }
</style>



<footer class="footer" id="footer">
  
  
    <div class="footer-list">
        &copy; 2021 • <a href="<?php $this->options->siteUrl(); ?>"><?php $this->options->title(); ?></a>
    </div>
    
    <div class="footer-list">
        <?php if ($this->options->recordNumber): ?>
            <a href="<?php $this->options->recordLink(); ?>" target="_blank">
                <?php $this->options->recordNumber(); ?> </a>
        <?php else: ?>
            <a href="https://beian.miit.gov.cn/" style="display:inline-block;text-decoration:none;height:20px;line-height:20px;" target="_blank"> 黔ICP备2022001847号-1 </a>
        <?php endif; ?>
    </div>
    
    <div class="footer-list">
        <a
                href="http://www.beian.gov.cn/portal/registerSystemInfo?recordcode=52050202001316"
                style="display:inline-block;text-decoration:none;height:20px;line-height:20px;" target="_blank">
            <img src="http://www.beian.gov.cn/img/new/gongan.png" height="60%">&nbsp;&nbsp;贵公网安备
                52050202001316号
        </a>
    </div>
    
     <div class="footer-list">
        <a class="item" href="<?php echo getHidePage($page, 'about'); ?>">关于我</a> |&nbsp;&nbsp; 
        <a class="item" href="<?php echo getHidePage($page, 'links'); ?>">友情链接</a> |&nbsp;&nbsp; 
        <a class="item" href="https://allms.cn/feed/" target="_blank">RSS订阅</a>
    </div>
  
    
</footer>
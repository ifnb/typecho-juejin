<style>
    .page-navigator {
      list-style: none;margin: 25px 0;padding: 0;text-align: center;
      font-size: 16px;  
    }
    .page-navigator li {
      display: inline-block;
      PADDING-RIGHT: 3px; PADDING-LEFT: 3px; PADDING-BOTTOM: 3px; MARGIN: 3px; PADDING-TOP: 3px; TEXT-ALIGN: center;
    }
    .page-navigator a {
      BORDER-RIGHT: #ddd 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #ddd 1px solid; PADDING-LEFT: 5px; PADDING-BOTTOM: 2px; BORDER-LEFT: #ddd 1px solid; COLOR: #88af3f; MARGIN-RIGHT: 2px; PADDING-TOP: 2px; BORDER-BOTTOM: #ddd 1px solid; TEXT-DECORATION: none;
    }
    .page-navigator a:hover {
      BORDER-RIGHT: #85bd1e 1px solid; BORDER-TOP: #85bd1e 1px solid; BORDER-LEFT: #85bd1e 1px solid; COLOR: #638425; BORDER-BOTTOM: #85bd1e 1px solid; BACKGROUND-COLOR: #f1ffd6;
    }
    .page-navigator a:active {
      BORDER-RIGHT: #85bd1e 1px solid; BORDER-TOP: #85bd1e 1px solid; BORDER-LEFT: #85bd1e 1px solid; COLOR: #638425; BORDER-BOTTOM: #85bd1e 1px solid; BACKGROUND-COLOR: #f1ffd6;
    }
    .page-navigator .current a {
      BORDER-RIGHT: #b2e05d 1px solid; PADDING-RIGHT: 5px; BORDER-TOP: #b2e05d 1px solid; PADDING-LEFT: 5px; FONT-WEIGHT: bold; PADDING-BOTTOM: 2px; BORDER-LEFT: #b2e05d 1px solid; COLOR: #fff; MARGIN-RIGHT: 2px; PADDING-TOP: 2px; BORDER-BOTTOM: #b2e05d 1px solid; BACKGROUND-COLOR: #b2e05d;
    }
</style>
<div class="left">
    <nav class="head head-nav">
        <a href="#" class="nav-item active">热门</a>
        <!--<a href="#" class="nav-item">最新</a>-->
        <!--<a href="#" class="nav-item">热榜</a>-->
    </nav>
    <?php $this->need("components/index/skeleton.php");?>
    <div id="article-list-wrap" class="hide">
        <?php while ($this->next()) : ?>
            <?php $this->need("components/default/mini-article-card.php");?>
        <?php endwhile; ?>
    </div>

    <!--无限加载-->
    <?php if ($this->options->pageMethod == 1): ?>
        <div class="auto-load <?php if (!$this->have()) echo 'show'; ?>">
            <?php $this->pageLink("上一页", 'prev'); ?>
            <?php $this->pageLink("下一页", 'next'); ?>
            <img src="<?php themeUrl('static/images/tool/section-loading.gif'); ?>" alt="加载中">
        </div>
    <?php endif; ?>

</div>
<!--普通分页-->
<?php if ($this->options->pageMethod == 0): ?>
    <?php $this->pageNav('&laquo; 前一页', '后一页 &raquo;'); ?>
<?php endif; ?>

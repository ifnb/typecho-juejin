<style>
    @-moz-keyframes loading {
        0%, 100% {
            -moz-transform: scale(1) rotateZ(0deg);
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        26% {
            -moz-transform: scale(1.1) rotateZ(12deg);
            transform: scale(1.1) rotateZ(12deg);
            opacity: .2;
        }

        76% {
            -moz-transform: scale(0.8) rotateZ(-8deg);
            transform: scale(0.8) rotateZ(-8deg);
            opacity: .6;
        }
    }

    @-webkit-keyframes loading {
        0%, 100% {
            -webkit-transform: scale(1) rotateZ(0deg);
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        26% {
            -webkit-transform: scale(1.1) rotateZ(12deg);
            transform: scale(1.1) rotateZ(12deg);
            opacity: .2;
        }

        76% {
            -webkit-transform: scale(0.8) rotateZ(-8deg);
            transform: scale(0.8) rotateZ(-8deg);
            opacity: .6;
        }
    }

    @keyframes loading {
        0%, 100% {
            -moz-transform: scale(1) rotateZ(0deg);
            -ms-transform: scale(1) rotateZ(0deg);
            -webkit-transform: scale(1) rotateZ(0deg);
            transform: scale(1) rotateZ(0deg);
            opacity: 1;
        }

        26% {
            -moz-transform: scale(1.1) rotateZ(12deg);
            -ms-transform: scale(1.1) rotateZ(12deg);
            -webkit-transform: scale(1.1) rotateZ(12deg);
            transform: scale(1.1) rotateZ(12deg);
            opacity: .2;
        }

        76% {
            -moz-transform: scale(0.8) rotateZ(-8deg);
            -ms-transform: scale(0.8) rotateZ(-8deg);
            -webkit-transform: scale(0.8) rotateZ(-8deg);
            transform: scale(0.8) rotateZ(-8deg);
            opacity: .6;
        }
    }

    .loader {
        overflow: hidden;
        font-size: 45px;
    }

    .loader span {
        -moz-animation: loading 1s linear infinite -0.8s;
        -webkit-animation: loading 1s linear infinite -0.8s;
        animation: loading 1s linear infinite -0.8s;
        float: left;
    }

    .span-center {
        display: flex;
        justify-content: center;
        align-content: center;
        flex-direction: column;
        /*align-items: center;*/
        background-color: #fff;
        border-radius: 2px;
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, .05);
        margin-bottom: 18px
    }
    .span-center .head {
        border-bottom: 1px solid rgba(150, 150, 150, .1);
        padding: 12px 16px
    }

    .span-center .head .title {
        color: #333;
        font-size: 16px;
        font-weight: 500
    }

    .loader-tags {
        overflow: hidden;
        font-family: Arial;
        font-size: 18px;
        line-height: 32px;
        font-weight: bold;
        margin-left: 10px;
    }

    .tag-div-li {
        float: left;
        list-style: none;
        display: block;
        list-style-type: none;
        white-space: nowrap;
    }

    .dujitang {
        overflow: hidden;
        font-family: Arial;
        font-size: 18px;
        line-height: 32px;
        font-weight: bold;
        margin-left: 10px;
    }

    /*关于作者start*/
    .span-center-about {
        display: flex;
        flex-direction: column;
    }

    .span-center-about .about-author {
        background-color: #fff;
        border-radius: 2px;
        box-shadow: 0 1px 2px 0 rgba(0, 0, 0, .05);
        margin-bottom: 18px
    }

    .span-center-about .about-author .head {
        border-bottom: 1px solid rgba(150, 150, 150, .1);
        padding: 12px 16px
    }

    .span-center-about .about-author .head .title {
        color: #333;
        font-size: 16px;
        font-weight: 500
    }

    .span-center-about .about-author .body {
        padding: 16px
    }

    .span-center-about .about-author .body .user-info {
        align-items: center;
        display: flex
    }

    .span-center-about .about-author .body .user-info .avatar {
        background-color: #eee;
        border-radius: 50%;
        flex-shrink: 0;
        height: 50px;
        margin-right: 12px;
        overflow: hidden;
        width: 50px
    }

    .span-center-about .about-author .body .user-info .avatar img {
        height: 100%;
        -o-object-fit: cover;
        object-fit: cover;
        vertical-align: top;
        width: 100%
    }

    .span-center-about .about-author .body .user-info .info {
        flex-grow: 1;
        min-width: 0
    }

    .span-center-about .about-author .body .user-info .info .name {
        color: #000;
        font-size: 16px;
        font-weight: 700;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap
    }

    .span-center-about .about-author .body .user-info .info .name a {
        color: inherit;
        text-decoration: none
    }

    .span-center-about .about-author .body .user-info .info .description {
        color: #72777b;
        font-size: 12px;
        margin-top: 10px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap
    }

    .span-center-about .about-author .body .stat-warp {
        margin-top: 16px
    }

    .span-center-about .about-author .body .stat-warp .stat-item {
        color: #000;
        font-size: 15px;
        line-height: 25px;
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap
    }

    .span-center-about .about-author .body .stat-warp .stat-item + .stat-item {
        margin-top: 10px
    }

    .span-center-about .about-author .body .stat-warp .stat-item .icon {
        background-color: #deefff;
        border-radius: 50%;
        color: #66baff;
        display: inline-block;
        font-size: inherit;
        height: 25px;
        line-height: 25px;
        margin-right: 12px;
        text-align: center;
        vertical-align: top;
        width: 25px
    }

    /*关于作者end*/

</style>

<div class="right">

    <!--站点信息-->
    <div class="span-center-about">
        <div class="about-author">
            <div class="head">
                <h2 class="title">关于作者</h2>
            </div>
            <div class="body">
                <div class="user-info">
                    <div class="avatar">
                        <?php $this->author->gravatar(80); ?>
                    </div>
                    <div class="info">
                        <h3 class="name"><a href="<?php $this->author->permalink(); ?>"><?php $this->author(); ?></a>
                        </h3>
                        <?php if ($this->options->siteSign): ?>
                            <!--个性签名-->
                            <div class="description"
                                 title="<?php $this->options->siteSign(); ?>"><?php $this->options->siteSign(); ?></div>
                        <?php else: ?>
                            <!--网站描述-->
                            <div class="description"
                                 title="<?php $this->options->description() ?>"><?php $this->options->description() ?></div>
                        <?php endif; ?>
                    </div>
                </div>
                <!--站点desc-->
                <div class="stat-warp">
                    <div class="stat-item">
                        <i class="icon icon-line-medalxunzhang-01"></i>站点职位 博主
                    </div>
                    <div class="stat-item">
                        <i class="icon icon-like-fill"></i>获得点赞 999
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--输出标签云-->
    <div class="span-center" style="margin-top: 30px;">
        <div class="head">
            <h2 class="title">标签</h2>
        </div>
        <div class="tag-div">
            <?php $this->widget('Widget_Metas_Tag_Cloud', 'ignoreZeroCount=1&limit=30')->to($tags); ?>
            <ul class="tags-list">
                <?php while ($tags->next()): ?>
                    <li class="tag-div-li"><a class="loader-tags"
                                              style="color: rgb(<?php echo(rand(0, 255)); ?>, <?php echo(rand(0, 255)); ?>, <?php echo(rand(0, 255)); ?>)"
                                              href="<?php $tags->permalink(); ?>"
                                              title='<?php $tags->name(); ?>'><?php $tags->name(); ?></a></li>
                <?php endwhile; ?>
            </ul>
        </div>

    </div>

    <!--毒鸡汤-->
    <div class="span-center" style="margin-top: 40px;">
        <div class="head">
            <h2 class="title">毒鸡汤</h2>
        </div>
        <?php
        function geturl($url)
        {
            $headerArray = array("Content-type:application/json;", "Accept:application/json");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
            $output = curl_exec($ch);
            curl_close($ch);
            $output = json_decode($output, true);
            return $output;
        }

        $dujitang = geturl("http://api.lkblog.net/ws/api.php");
        echo '<div class="dujitang">🎐' . $dujitang['data'] . '🎐</div>';
        ?>
    </div>

    <?php //footer?>
</div>
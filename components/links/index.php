<!--<!DOCTYPE HTML>-->
<!--<html lang="zh-CN">-->


<head>
<!--    <meta charset="UTF-8">-->
<!--    <meta name="viewport"-->
<!--          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">-->
<!--    <meta http-equiv="X-UA-Compatible" content="ie=edge">-->
<!--    <title>Document</title>-->
<!--    <link rel="stylesheet" type="text/css" href="css/awesome/css/all.css">-->
<!--    <link rel="stylesheet" type="text/css" href="css/materialize/materialize.min.css">-->
<!--    <link rel="stylesheet" type="text/css" href="css/animate/animate.min.css">-->
<!--    <link rel="stylesheet" type="text/css" href="css/matery.css">-->
<!--    <link rel="stylesheet" type="text/css" href="css/my.css">-->
<!--    <link rel="stylesheet" type="text/css" href="css/links.css">-->
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/awesome/css/all.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/materialize/materialize.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/animate/animate.min.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/matery.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/my.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php $this->options->themeUrl('components/links/css/links.css'); ?>">

</head>

<!--<body>-->
<main class="content" style="margin-top: 200px">
    <div class="container friends-container">
        <div class="card">
            <div class="card-content">
                <div class="tag-title center-align">
                    <i class="fas fa-address-book"></i>&nbsp;&nbsp;友情链接
                </div>
                <article id="friends-link">


                    <div class="row tags-posts friend-all">


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card1">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://image.luokangyuan.com/1_qq_27922023.jpg" alt="img">
                                        <div>
                                            <h1 class="friend-name">码酱</h1>
                                            <p style="position: relative;top: -35px;">
                                                我不是大佬，只是在追寻大佬的脚步</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="http://luokangyuan.com/" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去学习
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card2">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/20190601.png" alt="img">
                                        <div>
                                            <h1 class="friend-name">码农</h1>
                                            <p style="position: relative;top: -35px;">
                                                这里不隐含扭曲的价值观，而是整合并充盈正能量</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="https://www.90c.vip/" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去学习
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card3">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/2019/11/23/avatar2.png" alt="img">
                                        <div>
                                            <h1 class="friend-name">洪卫の博客</h1>
                                            <p style="position: relative;top: -35px;">凭寄狂夫书一纸，信在成都万里桥。</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="https://sunhwee.com" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去探索
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card4">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/2019/11/23/avatar.jpg" alt="img">
                                        <div>
                                            <h1 class="friend-name">过客~励む</h1>
                                            <p style="position: relative;top: -35px;">
                                                你现在的努力，是为了以后有更多的选择。</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="https://yafine-blog.cn" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去探索
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card5">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/2019/11/23/avatar5.png" alt="img">
                                        <div>
                                            <h1 class="friend-name">Sitoi</h1>
                                            <p style="position: relative;top: -35px;">妄想通过成为 Spider-Man
                                                来实现财富自由的程序猿</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="https://sitoi.cn" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去探索
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card6">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/2019/11/23/avatar3.jpeg" alt="img">
                                        <div>
                                            <h1 class="friend-name">Five-great</h1>
                                            <p style="position: relative;top: -35px;">
                                                有事多研究，没事瞎琢磨。(Five-great)</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="http://www.fivecc.cn/" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            前去探索
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col s12 m6 l4 friend-div" data-aos="zoom-in-up">
                            <div class="card frind-card7">
                                <div class="frind-ship">
                                    <div class="title">
                                        <img src="http://static.blinkfox.com/2019/12/24logo.png" alt="img">
                                        <div>
                                            <h1 class="friend-name">A2Data</h1>
                                            <p style="position: relative;top: -35px;">
                                                武术跨行大数据，用技术推动梦想的落地！</p>
                                        </div>
                                    </div>
                                    <div class="friend-button">
                                        <a href="https://www.a2data.cn" target="_blank"
                                           class="button button-glow button-rounded button-caution">
                                            开启跨行之旅
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                </article>
            </div>
        </div>
    </div>
</main>

<!--</body>-->

<!--</html>-->

<?php
/**
 * links
 *
 * @package custom
 */
if (!defined('__TYPECHO_ROOT_DIR__')) exit; ?>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <?php if (!is_ajax()): ?>
        <?php $this->need("components/default/head.php"); ?>
        <?php //样式?>
        <?php $this->need("dist/css/links.php"); ?>

    <?php endif; ?>
</head>
<body>
<?php if (!is_ajax()): ?>
    <?php $this->need("components/default/header.php"); ?>
    <?php $this->need("components/default/nav.php"); ?>
<?php endif; ?>
<style>
    /*#main .links-content .links-body ol li img, #main .links-content .links-body ul li img {*/
    /*    -o-object-fit: cover;*/
    /*    object-fit: cover;*/
    /*}*/
    /*#main .links-content .links-body ol, #main .links-content .links-body ul {*/
    /*    grid-auto-rows: auto;*/
    /*}*/
    /*#main .links-content .links-body ol li, #main .links-content .links-body ul li {*/
    /*    overflow: visible;*/
    /*    display: flex;*/
    /*    align-items: center;*/
    /*    flex-direction: column;*/
    /*}*/
    /*.links-body ul>li:hover{*/
    /*    background-color: rgba(0,0,0,0) !important;*/
    /*}*/

   /*.links-span{*/
   /*     text-align: center;*/
   /*     font-size: 16px;*/
   /*     font-family: fangsong;*/
   /*     font-weight: bold;*/
   /*     color: darkblue;*/
   /* }*/
</style>
<main id="main">
    <div class="container">
        <div class="links-content">
            <div class="links-head">
                <h1 class="title"><?php $this->title() ?><?php if ($this->user->hasLogin()) : ?><a
                        href="<?php $this->options->adminUrl(); ?>write-<?php if ($this->is('post')) : ?>post<?php else : ?>page<?php endif; ?>.php?cid=<?php echo $this->cid; ?>"
                        class="icon icon-edit" target="_blank"></a><?php endif; ?></h1>
            </div>
            <div class="links-body">
                <?php
                $db = Typecho_Db::get();
                $sql = $db->select()->from('table.comments')
                    ->where('cid = ?', $this->cid)
                    ->where('mail = ?', $this->remember('mail', true))
                    ->where('status = ?', 'approved')
                    //只有通过审核的评论才能看回复可见内容
                    ->limit(1);
                $result = $db->fetchAll($sql);
                $content = $this->content;
                //a链接增加_blank
                //$content = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/sm', '<a href="$1" target="_blank">$2</a><span class="links-span">$2</span>', $content);
                // $content = preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/sm', '<span class="links-span">$2</span>', $content);
                echo $content
                ?>
            </div>
            <div class="links-declaration">
                <div class="left">
                    <i class="icon icon-warning-circle-fill"></i>
                </div>
                <div class="right">
                    <h2 class="title">作者寄语</h2>
                     <p class="subtitle">听听音乐,搞搞代码,看看电影,享受时光吧！</p>
                </div>
            </div>
        </div>
    </div>
</main>


<?php //登录弹窗?>
<?php $this->need("components/default/login-dialog.php"); ?>
<?php //搜索抽屉?>
<?php $this->need("components/default/drawer-search.php"); ?>
<?php //悬浮工具?>
<?php $this->need("components/default/fixed-tool.php"); ?>
<?php //脚本?>
<?php $this->need("dist/script/links.php"); ?>
</body>
</html>